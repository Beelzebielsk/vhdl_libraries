Library ieee;
use ieee.std_logic_1164.all;

entity D_latch_gated is
	port(
		D      : in std_logic := '0';
		ENABLE : in std_logic := '0';
		P      : out std_logic := '0';
		Q      : out std_logic := '1'
	);
end entity D_latch_gated;

architecture uses_buffer of D_latch_gated is

	component SR_Latch_gated
		port(
			S, R : in std_logic := '0';
			ENABLE : in std_logic := '0';
			P : buffer std_logic := '0';
			Q : buffer std_logic := '1'
		);
	end component;

	signal not_D : std_logic := '1';
	-- Opposite of starting value of D.
begin
	not_D <= not D;
	underlying_latch: SR_Latch_gated
		port map(
			S => D,
			R => not_D,
			ENABLE => ENABLE,
			P => P,
			Q => Q
		);
end uses_buffer;
