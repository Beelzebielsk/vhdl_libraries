------------------------------------------------------------
-- lib_package: {{{
------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;

package memory is

component D_flip_flop_async_reset is
	port(
		D : in std_logic := '0';
		CLK : in std_logic := '1';
		A_RESET : in std_logic := '1';
		-- Active low asynchronous reset.
		-- Starts off.
		q : out std_logic := '0';
		qnot : out std_logic := '1'
	);
end component D_flip_flop_async_reset;
component D_flip_flop is
	port(
		D : in std_logic := '0';
		CLK : in std_logic := '0';
		q : out std_logic := '0';
		qnot : out std_logic := '1'
		-- Make sure that q and qnot
		-- start in some predetermined state
		-- which will behave predictably.
		-- They behave predictably whenever
		-- they are the opposite of each other.
	);
end component D_flip_flop;
component D_gated_latch_async_reset is
	port(
		D : in std_logic := '0';
		ENABLE : in std_logic := '0';
		A_RESET : in std_logic := '1';
		P : out std_logic := '0';
		Q : out std_logic := '1'
	);
end component D_gated_latch_async_reset;
component D_latch_gated is
	port(
		D      : in std_logic := '0';
		ENABLE : in std_logic := '0';
		P      : out std_logic := '0';
		Q      : out std_logic := '1'
	);
end component D_latch_gated;
component generic_decoder is
	generic ( NUM_SELECT : natural := 4 );
	port(
		select_inputs : in std_logic_vector( NUM_SELECT - 1 downto 0);
		ENABLE        : in std_logic := '0';
		-- Active high enable.
		select_lines  : out std_logic_vector( 2**NUM_SELECT - 1 downto 0)
	);
end component generic_decoder;
component SR_flip_flop_async_reset is
	port(
		S : in std_logic := '0';
		R : in std_logic := '0';
		CLK : in std_logic := '1';
		-- Leading edge triggered flip flop.
		-- Start in master disabled mode.
		A_RESET : in std_logic := '1';
		-- Active low asynchronous reset.
		-- Starts off.
		q : buffer std_logic := '0';
		qnot : buffer std_logic := '1'
		-- Start in reset state, hold mode.
	);
end component SR_flip_flop_async_reset;
component SR_gated_latch_async_reset is
	port(
		S : in std_logic := '0';
		R : in std_logic := '0';
		ENABLE : in std_logic := '0';
		-- Active high enable.
		A_RESET : in std_logic := '1';
		-- Active low asynchronus reset.
		P : buffer std_logic := '0';
		Q : buffer std_logic := '1'
		-- Starts in Reset mode.
	);
end component SR_gated_latch_async_reset;
component SR_Latch_gated is
	port(
		S, R : in std_logic := '0';
		ENABLE : in std_logic := '0';
		P : buffer std_logic := '0';
		Q : buffer std_logic := '1'
	);
end component SR_Latch_gated;
component T_flip_flop is
	port(
		T : in std_logic := '0';
		CLK : in std_logic := '1';
		q : inout std_logic := '0';
		qnot : out std_logic := '1'
	);
end component T_flip_flop;

function clock_start (leading_edge : boolean) return std_logic;
end package memory;

package body memory is
	function clock_start (leading_edge : boolean) return std_logic is
	begin
		case leading_edge is
			when true => return '1';
			when false => return '0';
		end case;
	end function clock_start;
end package body memory;
------------------------------------------------------------
-- }}}
------------------------------------------------------------

------------------------------------------------------------
-- D_gated_latch_async_reset: {{{
------------------------------------------------------------
Library ieee;
use ieee.std_logic_1164.all;

entity D_gated_latch_async_reset is
	port(
		D : in std_logic := '0';
		ENABLE : in std_logic := '0';
		A_RESET : in std_logic := '1';
		P : out std_logic := '0';
		Q : out std_logic := '1'
	);
end entity D_gated_latch_async_reset;

architecture arch of D_gated_latch_async_reset is
	component SR_gated_latch_async_reset
		port(
			S : in std_logic := '0';
			R : in std_logic := '0';
			ENABLE : in std_logic := '0';
			A_RESET : in std_logic := '1';
			P : buffer std_logic := '0';
			Q : buffer std_logic := '1'
		);
	end component;
	signal notD : std_logic := '1';
begin
	notD <= not D;
	inner_latch : SR_gated_latch_async_reset
		port map(
			S => D,
			R => notD,
			ENABLE => ENABLE,
			A_RESET => A_RESET,
			P => P,
			Q => Q
		);
end arch;

------------------------------------------------------------
-- }}}
------------------------------------------------------------

------------------------------------------------------------
-- D_Latch_gated: {{{
------------------------------------------------------------
Library ieee;
use ieee.std_logic_1164.all;

entity D_latch_gated is
	port(
		D      : in std_logic := '0';
		ENABLE : in std_logic := '0';
		P      : out std_logic := '0';
		Q      : out std_logic := '1'
	);
end entity D_latch_gated;

architecture uses_buffer of D_latch_gated is

	component SR_Latch_gated
		port(
			S, R : in std_logic := '0';
			ENABLE : in std_logic := '0';
			P : buffer std_logic := '0';
			Q : buffer std_logic := '1'
		);
	end component;

	signal not_D : std_logic := '1';
	-- Opposite of starting value of D.
begin
	not_D <= not D;
	underlying_latch: SR_Latch_gated
		port map(
			S => D,
			R => not_D,
			ENABLE => ENABLE,
			P => P,
			Q => Q
		);
end uses_buffer;
------------------------------------------------------------
-- }}}
------------------------------------------------------------

------------------------------------------------------------
-- SR_gated_latch_async_reset: {{{
------------------------------------------------------------
Library ieee;
use ieee.std_logic_1164.all;

entity SR_gated_latch_async_reset is
	port(
		S : in std_logic := '0';
		R : in std_logic := '0';
		ENABLE : in std_logic := '0';
		-- Active high enable.
		A_RESET : in std_logic := '1';
		-- Active low asynchronus reset.
		P : buffer std_logic := '0';
		Q : buffer std_logic := '1'
		-- Starts in Reset mode.
	);
end entity SR_gated_latch_async_reset;

architecture arch of SR_gated_latch_async_reset is
	signal gated_S : std_logic := '1';
	signal gated_R : std_logic := '1';
	-- Again, start in reset mode.
begin
	gated_S <= S nand ENABLE;
	gated_R <= R nand ENABLE;
	P <= A_RESET and (gated_S nand Q);
	Q <= (not A_RESET) or (gated_R nand P);
end arch;

-- ASYNC Reset equations:
-- Active low Reset, so when A_RESET=1, then:
	-- P <= gated_S nand Q
	-- Q <= gated_R nand P
-- When A_RESET=0 then:
	-- P <= 0
	-- Q <= 1
-- So, ultimately:
	-- P* (gated_S, A_RESET, Q) = A_RESET op (gated_S nand Q)
		-- and 'op' is an operation such that:
		-- P* (gated_S, 1, Q) = gated_S nand Q
		-- P* (gated_S, 0, Q) = 0
	-- Q* (gated_S, A_RESET, P) = A_RESET op (gated_R nand P)
		-- and 'op' is an operation such that:
		-- Q* (gated_S, 1 , P) = gated_R nand P
		-- Q* (gated_S, 0 , P) = 1
-- The 'op' in these two functions behave similarly to
-- 'and' and 'or' functions. Of the two possible values
-- that an argument can take-- '0' or '1', one of them
-- is an identity and the other dominates. So I can just
-- choose an 'and' or 'or' function that fits the
-- constraints that I have.
	-- For Both P and Q:
		-- '1' must be an identity.
		-- '0' must dominate
	-- For P:
		-- '0' simply dominates. P* (gated_S, 0, Q) = 0.
		-- Therefore, I can make 'op' the 'and' function.
		-- P* <= A_RESET and (gated_S nand Q)
		-- P* <= 1 and (gated_S nand Q) = gated_S nand Q
		-- P* <= 0 and (gated_S nand Q) = 0
		-- Works!
	-- For Q:
		-- While '0' is supposed to dominate, it's supposed
		-- to produce a '1'. Also, '1' is supposed to be an
		-- identity. The 'or' function would work if '0' were
		-- supposed to be the identity and '1' the dominating
		-- value. However, that's easy to fix: negate A_RESET.
		-- Q* <= (not A_RESET) or (gated_S nand P)
		-- Q* <= (not 1) or (gated_S nand P) = 0 or gated_S nand P = gated_S nand P
		-- Q* <= (not 0) or (gated_S nand P) = 1 or gated_S nand P = 1
		-- Works!
------------------------------------------------------------
-- }}}
------------------------------------------------------------

------------------------------------------------------------
-- D_flip_flop_async_reset: {{{
------------------------------------------------------------
Library ieee;
use ieee.std_logic_1164.all;

entity D_flip_flop_async_reset is
	port(
		D : in std_logic := '0';
		CLK : in std_logic := '1';
		A_RESET : in std_logic := '1';
		-- Active low asynchronous reset.
		-- Starts off.
		q : out std_logic := '0';
		qnot : out std_logic := '1'
	);
end entity D_flip_flop_async_reset;

architecture arch of D_flip_flop_async_reset is
	component D_gated_latch_async_reset
		port(
			D : in std_logic := '0';
			ENABLE : in std_logic := '0';
			A_RESET : in std_logic := '1';
			P : out std_logic := '0';
			Q : out std_logic := '1'
		);
	end component;
	signal master_out_q : std_logic := '0';
	signal notCLK : std_logic := '0';
begin
	notCLK <= not CLK;
	
	master: D_gated_latch_async_reset
		port map(
			D => D,
			ENABLE => notCLK,
			A_RESET => A_RESET,
			P => master_out_q
		);

	slave: D_gated_latch_async_reset
		port map(
			D => master_out_q,
			ENABLE => CLK,
			A_RESET => A_RESET,
			P => q,
			Q => qnot
		);
end arch;
------------------------------------------------------------
-- }}}
------------------------------------------------------------

------------------------------------------------------------
-- T_flip_flop: {{{
------------------------------------------------------------
Library ieee;
use ieee.std_logic_1164.all;

-- Leading edge triggered.
entity T_flip_flop is
	port(
		T : in std_logic := '0';
		CLK : in std_logic := '1';
		q : inout std_logic := '0';
		qnot : out std_logic := '1'
	);
end entity T_flip_flop;

architecture arch of T_flip_flop is

	component D_flip_flop 
		port(
			D : in std_logic := '0';
			CLK : in std_logic := '0';
			q : out std_logic := '0';
			qnot : out std_logic := '1'
			-- Make sure that q and qnot
			-- start in some predetermined state
			-- which will behave predictably.
			-- They behave predictably whenever
			-- they are the opposite of each other.
		);
	end component D_flip_flop;

	signal realT : std_logic := '0';
begin
	realT <= T xor q;

	underlying_flip_flop : D_flip_flop
		port map(
			D => realT,
			CLK => CLK,
			q => q,
			qnot => qnot
		);

end arch;

------------------------------------------------------------
-- }}}
------------------------------------------------------------

------------------------------------------------------------
-- D_flip_flop: {{{
------------------------------------------------------------
Library ieee;
use ieee.std_logic_1164.all;
use work.memory.clock_start;

-- This flip flop is leading edge triggered.  Therefore, the master should be
-- enabled on 0 and the slave should be enabled on 1.  And the two should not both
-- be enabled at the same time, with a very small exception in time called the
-- "leading edge".  of the clock signal.
-- NOTE: The output of CLK goes straight to the slave in Gertner's designs,
-- rather than straight to the master, as in my textbook's design. So CLK 
-- should start at whatever enables the slave, so that NOT_CLK starts at
-- whatever disables the master.

entity D_flip_flop is
	generic(
		leading_edge : boolean := false
	);
	port(
		D : in std_logic := '0';
		CLK : in std_logic := clock_start(leading_edge);
		q : out std_logic := '0';
		qnot : out std_logic := '1'
		-- Make sure that q and qnot
		-- start in some predetermined state
		-- which will behave predictably.
		-- They behave predictably whenever
		-- they are the opposite of each other.
	);
end entity D_flip_flop;

architecture arch of D_flip_flop is

	component D_Latch_gated 
		port(
			D      : in std_logic := '0';
			ENABLE : in std_logic := '0';
			P      : buffer std_logic := '0';
			Q      : buffer std_logic := '1'
		);
 end component D_Latch_gated;

	signal NOT_CLK : std_logic := not CLK;
	signal master_clk, slave_clk : std_logic;
	signal master_out : std_logic := '0';
begin
	NOT_CLK <= not CLK;

	trailing_master_clock : if leading_edge = true generate
		master_clk <= NOT_CLK;
		slave_clk  <= CLK;
	end generate trailing_master_clock;

	leading_master_clock : if leading_edge = false generate
		master_clk <= CLK;
		slave_clk  <= NOT_CLK;
	end generate leading_master_clock;

	master : D_Latch_gated
		port map(
			D => D,
			ENABLE => master_clk,
			P => master_out
		);

	slave : D_Latch_gated
		port map(
			D => master_out,
			ENABLE => slave_clk,
			P => q,
			Q => qnot
		);
end arch;
-- verified in modelsim.

------------------------------------------------------------
-- }}}
------------------------------------------------------------

------------------------------------------------------------
-- SR_Latch_gated: {{{
------------------------------------------------------------
Library ieee;
use ieee.std_logic_1164.all;

entity SR_Latch_gated is
	port(
		S, R : in std_logic := '0';
		ENABLE : in std_logic := '0';
		P : buffer std_logic := '0';
		Q : buffer std_logic := '1'
	);
end entity SR_Latch_gated;

architecture uses_buffer of SR_Latch_gated is
	signal gated_S, gated_R : std_logic := '1';
	-- The gated_S and gated_R gates should be '1'
	-- in order to make the following true at the
	-- start:
	-- P* <= not(Q)
	-- and
	-- Q* <= not(P)
		-- P* <= gated_S nand Q
		--	  <= not(gated_S) or not(Q)
		--		<= not(1) or not(Q)
		--		<= 0 or not(Q)
		--		<= not(Q)
		-- Q* <= gated_R nand P
		--		<= not(gated_R) or not(P)
		--		<= not(1) or not(P)
		--		<= 0 or not(P)
		--		<= not(P)
begin
	gated_S <= S nand ENABLE;
	gated_R <= R nand ENABLE;
	P <= gated_S nand Q;
	Q <= gated_R nand P;
end uses_buffer;

-- Ungated:
-- P* <= S nor Q
-- P* <= not(S) and not(Q)
-- Q* <= R nor P
-- Q* <= not(R) and not(P)

-- Gated:
-- P* <= gated_S nand Q
-- P* <= not(gated_S) or not(Q)
-- P* <= not(S nand ENABLE) or not(Q) 
-- P* <= (S and ENABLE) or not(Q)

-- Q* <= gated_R nand P
-- Q* <= (R and ENABLE) or not(P)

-- When ENABLE = 0
	-- P* <= not(Q)
	-- Q* <= not(P)
-- When ENABLE = 1
	-- P* <= R or not(Q)
	-- Q* <= S or not(P)
		-- When S = R = 0:
			-- P* <= not(Q)
			-- Q* <= not(P)
		-- When S = 1, R = 0
			-- P* <= S or not(Q) <= 1
			-- Q* <= R or not(P) <= not(P)
			-- P** <= S or not(Q*) <= 1
			-- Q** <= R or not(P*) <= 0 or 0 < = 0
		-- When S = 0, R = 1
			-- P* <= S or not(Q) <= not(Q)
			-- Q* <= R or not(P) <= 1
			-- P** <= S or not(Q*) <= 0
			-- Q** <= R or not(P*) <= 1
		-- When S = R = 1
			-- P* <= S or not(Q) <= 1
			-- Q* <= R or not(P) <= 1
			-- Problems.
------------------------------------------------------------
-- }}}
------------------------------------------------------------

