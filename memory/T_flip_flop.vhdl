Library ieee;
use ieee.std_logic_1164.all;

-- Leading edge triggered.
entity T_flip_flop is
	generic(
		leading_edge : boolean := false
	);
	port(
		T : in std_logic := '0';
		CLK : in std_logic := '1';
		q : inout std_logic := '0';
		qnot : out std_logic := '1'
	);
end entity T_flip_flop;

architecture arch of T_flip_flop is

	component D_flip_flop 
		generic(
			leading_edge : boolean := false
		);
		port(
			D : in std_logic := '0';
			CLK : in std_logic := '0';
			q : out std_logic := '0';
			qnot : out std_logic := '1'
			-- Make sure that q and qnot
			-- start in some predetermined state
			-- which will behave predictably.
			-- They behave predictably whenever
			-- they are the opposite of each other.
		);
	end component D_flip_flop;

	signal realT : std_logic := '0';
begin
	realT <= T xor q;

	underlying_flip_flop : D_flip_flop
		port map(
			D => realT,
			CLK => CLK,
			q => q,
			qnot => qnot
		);

end arch;
