# File to be made.
output_file         := final_package.vhdl
library_description := lib_package.vhdl
phony_description   := $(library_description:.vhdl=.phony)
# Excluding lib_package here to force it to be processed first later.
# To work best, must be at top of output package file. This way, functions
# can be declared in 'lib_package' that components can use.
exclude_files       := 	$(output_file)\
												$(wildcard *test*.vhd*)\
												$(wildcard *example*.vhd*)
sources             := $(filter-out $(exclude_files),\
																		$(wildcard *.vhd)\
																		$(wildcard *.vhdl))

# Need to work with these two separately, unfortunately. Can't make a pattern
# to cover regex '.*.vhdl?'.
# Found easiest way to concatenate a bunch of prerequisite files into
# one target file was to make phony versions of the prequisites, and
# turn them into targets for a rule that just performs concatenation.
vhd_ending       := $(filter %.vhd, $(sources))
vhdl_ending      := $(filter %.vhdl, $(sources))
phony_vhd_files  := $(vhd_ending:.vhd=.phony)
phony_vhdl_files := $(vhdl_ending:.vhdl=.phony)
phony_files      := $(phony_vhd_files) $(phony_vhdl_files)

# Some stuff to insert:
separator     := ------------------------------------------------------------
open_section  := {{{
close_section := }}}

# Requires a package description to describe all the parts. Without it
# nothing works.
.PHONY: all
all : $(library_description)
	rm -f $(output_file)
	make $(output_file)

$(output_file) : $(phony_description) $(phony_files)

# Writes file, then writes a blank line.
.PHONY : $(phony_files)
$(phony_vhd_files)  : %.phony : %.vhd
	@echo "Making $@:"
	@echo -e "$(separator)\n-- $(basename $@): $(open_section)\n$(separator)" >> $(output_file)
	@cat $^ >> $(output_file)
	@echo -e "$(separator)\n-- $(close_section)\n$(separator)" >> $(output_file)
	@echo "" >> $(output_file)

$(phony_vhdl_files) : %.phony : %.vhdl
	@echo "Making $@:"
	@echo -e "$(separator)\n-- $(basename $@): $(open_section)\n$(separator)" >> $(output_file)
	@cat $^ >> $(output_file)
	@echo -e "$(separator)\n-- $(close_section)\n$(separator)" >> $(output_file)
	@echo "" >> $(output_file)

.PHONY : test
test :
	@echo Source Files:
	@echo $(sources)
	@echo Phony Files:
	@echo $(phony_files)
	@echo Excluded Files:
	@echo $(exclude_files)
