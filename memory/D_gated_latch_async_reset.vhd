Library ieee;
use ieee.std_logic_1164.all;

entity D_gated_latch_async_reset is
	port(
		D : in std_logic := '0';
		ENABLE : in std_logic := '0';
		A_RESET : in std_logic := '1';
		P : out std_logic := '0';
		Q : out std_logic := '1'
	);
end entity D_gated_latch_async_reset;

architecture arch of D_gated_latch_async_reset is
	component SR_gated_latch_async_reset
		port(
			S : in std_logic := '0';
			R : in std_logic := '0';
			ENABLE : in std_logic := '0';
			A_RESET : in std_logic := '1';
			P : buffer std_logic := '0';
			Q : buffer std_logic := '1'
		);
	end component;
	signal notD : std_logic := '1';
begin
	notD <= not D;
	inner_latch : SR_gated_latch_async_reset
		port map(
			S => D,
			R => notD,
			ENABLE => ENABLE,
			A_RESET => A_RESET,
			P => P,
			Q => Q
		);
end arch;

