LIBRARY ieee;
USE ieee.std_logic_1164.all;

package memory is

component D_flip_flop_async_reset is
	port(
		D : in std_logic := '0';
		CLK : in std_logic := '1';
		A_RESET : in std_logic := '1';
		-- Active low asynchronous reset.
		-- Starts off.
		q : out std_logic := '0';
		qnot : out std_logic := '1'
	);
end component D_flip_flop_async_reset;
component D_flip_flop is
	port(
		D : in std_logic := '0';
		CLK : in std_logic := '0';
		q : out std_logic := '0';
		qnot : out std_logic := '1'
		-- Make sure that q and qnot
		-- start in some predetermined state
		-- which will behave predictably.
		-- They behave predictably whenever
		-- they are the opposite of each other.
	);
end component D_flip_flop;
component D_gated_latch_async_reset is
	port(
		D : in std_logic := '0';
		ENABLE : in std_logic := '0';
		A_RESET : in std_logic := '1';
		P : out std_logic := '0';
		Q : out std_logic := '1'
	);
end component D_gated_latch_async_reset;
component D_latch_gated is
	port(
		D      : in std_logic := '0';
		ENABLE : in std_logic := '0';
		P      : out std_logic := '0';
		Q      : out std_logic := '1'
	);
end component D_latch_gated;
component generic_decoder is
	generic ( NUM_SELECT : natural := 4 );
	port(
		select_inputs : in std_logic_vector( NUM_SELECT - 1 downto 0);
		ENABLE        : in std_logic := '0';
		-- Active high enable.
		select_lines  : out std_logic_vector( 2**NUM_SELECT - 1 downto 0)
	);
end component generic_decoder;
component SR_flip_flop_async_reset is
	port(
		S : in std_logic := '0';
		R : in std_logic := '0';
		CLK : in std_logic := '1';
		-- Leading edge triggered flip flop.
		-- Start in master disabled mode.
		A_RESET : in std_logic := '1';
		-- Active low asynchronous reset.
		-- Starts off.
		q : buffer std_logic := '0';
		qnot : buffer std_logic := '1'
		-- Start in reset state, hold mode.
	);
end component SR_flip_flop_async_reset;
component SR_gated_latch_async_reset is
	port(
		S : in std_logic := '0';
		R : in std_logic := '0';
		ENABLE : in std_logic := '0';
		-- Active high enable.
		A_RESET : in std_logic := '1';
		-- Active low asynchronus reset.
		P : buffer std_logic := '0';
		Q : buffer std_logic := '1'
		-- Starts in Reset mode.
	);
end component SR_gated_latch_async_reset;
component SR_Latch_gated is
	port(
		S, R : in std_logic := '0';
		ENABLE : in std_logic := '0';
		P : buffer std_logic := '0';
		Q : buffer std_logic := '1'
	);
end component SR_Latch_gated;
component T_flip_flop is
	port(
		T : in std_logic := '0';
		CLK : in std_logic := '1';
		q : inout std_logic := '0';
		qnot : out std_logic := '1'
	);
end component T_flip_flop;

function clock_start (leading_edge : boolean) return std_logic;
end package memory;

package body memory is
	function clock_start (leading_edge : boolean) return std_logic is
	begin
		case leading_edge is
			when true => return '1';
			when false => return '0';
		end case;
	end function clock_start;
end package body memory;
