Library ieee;
use ieee.std_logic_1164.all;

entity SR_Latch_gated is
	port(
		S, R : in std_logic := '0';
		ENABLE : in std_logic := '0';
		P : buffer std_logic := '0';
		Q : buffer std_logic := '1'
	);
end entity SR_Latch_gated;

architecture uses_buffer of SR_Latch_gated is
	signal gated_S, gated_R : std_logic := '1';
	-- The gated_S and gated_R gates should be '1'
	-- in order to make the following true at the
	-- start:
	-- P* <= not(Q)
	-- and
	-- Q* <= not(P)
		-- P* <= gated_S nand Q
		--	  <= not(gated_S) or not(Q)
		--		<= not(1) or not(Q)
		--		<= 0 or not(Q)
		--		<= not(Q)
		-- Q* <= gated_R nand P
		--		<= not(gated_R) or not(P)
		--		<= not(1) or not(P)
		--		<= 0 or not(P)
		--		<= not(P)
begin
	gated_S <= S nand ENABLE;
	gated_R <= R nand ENABLE;
	P <= gated_S nand Q;
	Q <= gated_R nand P;
end uses_buffer;

-- Ungated:
-- P* <= S nor Q
-- P* <= not(S) and not(Q)
-- Q* <= R nor P
-- Q* <= not(R) and not(P)

-- Gated:
-- P* <= gated_S nand Q
-- P* <= not(gated_S) or not(Q)
-- P* <= not(S nand ENABLE) or not(Q) 
-- P* <= (S and ENABLE) or not(Q)

-- Q* <= gated_R nand P
-- Q* <= (R and ENABLE) or not(P)

-- When ENABLE = 0
	-- P* <= not(Q)
	-- Q* <= not(P)
-- When ENABLE = 1
	-- P* <= R or not(Q)
	-- Q* <= S or not(P)
		-- When S = R = 0:
			-- P* <= not(Q)
			-- Q* <= not(P)
		-- When S = 1, R = 0
			-- P* <= S or not(Q) <= 1
			-- Q* <= R or not(P) <= not(P)
			-- P** <= S or not(Q*) <= 1
			-- Q** <= R or not(P*) <= 0 or 0 < = 0
		-- When S = 0, R = 1
			-- P* <= S or not(Q) <= not(Q)
			-- Q* <= R or not(P) <= 1
			-- P** <= S or not(Q*) <= 0
			-- Q** <= R or not(P*) <= 1
		-- When S = R = 1
			-- P* <= S or not(Q) <= 1
			-- Q* <= R or not(P) <= 1
			-- Problems.
