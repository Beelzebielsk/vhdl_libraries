Library ieee;
use ieee.std_logic_1164.all;
use work.memory.clock_start;

entity D_flip_flop_async_reset is
	generic(
		leading_edge : boolean := true
	);
	port(
		D : in std_logic := '0';
		CLK : in std_logic := clock_start(leading_edge);
		A_RESET : in std_logic := '1';
		-- Active low asynchronous reset.
		-- Starts off.
		q : out std_logic := '0';
		qnot : out std_logic := '1'
	);
end entity D_flip_flop_async_reset;

architecture arch of D_flip_flop_async_reset is
	component D_gated_latch_async_reset
		port(
			D : in std_logic := '0';
			ENABLE : in std_logic := '0';
			A_RESET : in std_logic := '1';
			P : out std_logic := '0';
			Q : out std_logic := '1'
		);
	end component;
	signal master_out_q : std_logic := '0';
	signal notCLK : std_logic := not CLK;
	signal master_clk, slave_clk : std_logic;
begin
	notCLK <= not CLK;

	-- Master enabled to slave enabled is a write, so for 0 -> 1 to mean a write
	-- the master latch has to have a negated clock.
	leading_master_clock : if leading_edge = true generate
		master_clk <= NOT_CLK;
		slave_clk  <= CLK;
	end generate leading_master_clock;

	-- Opposite of above.
	trailing_master_clock : if leading_edge = false generate
		master_clk <= CLK;
		slave_clk  <= NOT_CLK;
	end generate trailing_master_clock;
	
	master: D_gated_latch_async_reset
		port map(
			D => D,
			ENABLE => master_clk,
			A_RESET => A_RESET,
			P => master_out_q
		);

	slave: D_gated_latch_async_reset
		port map(
			D => master_out_q,
			ENABLE => slave_clk,
			A_RESET => A_RESET,
			P => q,
			Q => qnot
		);
end arch;
