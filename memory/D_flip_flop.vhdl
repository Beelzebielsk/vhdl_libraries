Library ieee;
use ieee.std_logic_1164.all;
use work.memory.clock_start;

-- This flip flop is leading edge triggered.  Therefore, the master should be
-- enabled on 0 and the slave should be enabled on 1.  And the two should not both
-- be enabled at the same time, with a very small exception in time called the
-- "leading edge".  of the clock signal.
-- NOTE: The output of CLK goes straight to the slave in Gertner's designs,
-- rather than straight to the master, as in my textbook's design. So CLK 
-- should start at whatever enables the slave, so that NOT_CLK starts at
-- whatever disables the master.

entity D_flip_flop is
	generic(
		leading_edge : boolean := false
	);
	port(
		D : in std_logic := '0';
		CLK : in std_logic := clock_start(leading_edge);
		q : out std_logic := '0';
		qnot : out std_logic := '1'
		-- Make sure that q and qnot
		-- start in some predetermined state
		-- which will behave predictably.
		-- They behave predictably whenever
		-- they are the opposite of each other.
	);
end entity D_flip_flop;

architecture arch of D_flip_flop is

	component D_Latch_gated 
		port(
			D      : in std_logic := '0';
			ENABLE : in std_logic := '0';
			P      : buffer std_logic := '0';
			Q      : buffer std_logic := '1'
		);
 end component D_Latch_gated;

	signal NOT_CLK : std_logic := not CLK;
	signal master_clk, slave_clk : std_logic;
	signal master_out : std_logic := '0';
begin
	NOT_CLK <= not CLK;

	-- Master enabled to slave enabled is a write, so for 0 -> 1 to mean a write
	-- the master latch has to have a negated clock.
	leading_master_clock : if leading_edge = true generate
		master_clk <= NOT_CLK;
		slave_clk  <= CLK;
	end generate leading_master_clock;

	-- Opposite of above.
	trailing_master_clock : if leading_edge = false generate
		master_clk <= CLK;
		slave_clk  <= NOT_CLK;
	end generate trailing_master_clock;

	master : D_Latch_gated
		port map(
			D => D,
			ENABLE => master_clk,
			P => master_out
		);

	slave : D_Latch_gated
		port map(
			D => master_out,
			ENABLE => slave_clk,
			P => q,
			Q => qnot
		);
end arch;
-- verified in modelsim.

