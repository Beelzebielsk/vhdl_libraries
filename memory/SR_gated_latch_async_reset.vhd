Library ieee;
use ieee.std_logic_1164.all;

entity SR_gated_latch_async_reset is
	port(
		S : in std_logic := '0';
		R : in std_logic := '0';
		ENABLE : in std_logic := '0';
		-- Active high enable.
		A_RESET : in std_logic := '1';
		-- Active low asynchronus reset.
		P : buffer std_logic := '0';
		Q : buffer std_logic := '1'
		-- Starts in Reset mode.
	);
end entity SR_gated_latch_async_reset;

architecture arch of SR_gated_latch_async_reset is
	signal gated_S : std_logic := '1';
	signal gated_R : std_logic := '1';
	-- Again, start in reset mode.
begin
	gated_S <= S nand ENABLE;
	gated_R <= R nand ENABLE;
	P <= A_RESET and (gated_S nand Q);
	Q <= (not A_RESET) or (gated_R nand P);
end arch;

-- ASYNC Reset equations:
-- Active low Reset, so when A_RESET=1, then:
	-- P <= gated_S nand Q
	-- Q <= gated_R nand P
-- When A_RESET=0 then:
	-- P <= 0
	-- Q <= 1
-- So, ultimately:
	-- P* (gated_S, A_RESET, Q) = A_RESET op (gated_S nand Q)
		-- and 'op' is an operation such that:
		-- P* (gated_S, 1, Q) = gated_S nand Q
		-- P* (gated_S, 0, Q) = 0
	-- Q* (gated_S, A_RESET, P) = A_RESET op (gated_R nand P)
		-- and 'op' is an operation such that:
		-- Q* (gated_S, 1 , P) = gated_R nand P
		-- Q* (gated_S, 0 , P) = 1
-- The 'op' in these two functions behave similarly to
-- 'and' and 'or' functions. Of the two possible values
-- that an argument can take-- '0' or '1', one of them
-- is an identity and the other dominates. So I can just
-- choose an 'and' or 'or' function that fits the
-- constraints that I have.
	-- For Both P and Q:
		-- '1' must be an identity.
		-- '0' must dominate
	-- For P:
		-- '0' simply dominates. P* (gated_S, 0, Q) = 0.
		-- Therefore, I can make 'op' the 'and' function.
		-- P* <= A_RESET and (gated_S nand Q)
		-- P* <= 1 and (gated_S nand Q) = gated_S nand Q
		-- P* <= 0 and (gated_S nand Q) = 0
		-- Works!
	-- For Q:
		-- While '0' is supposed to dominate, it's supposed
		-- to produce a '1'. Also, '1' is supposed to be an
		-- identity. The 'or' function would work if '0' were
		-- supposed to be the identity and '1' the dominating
		-- value. However, that's easy to fix: negate A_RESET.
		-- Q* <= (not A_RESET) or (gated_S nand P)
		-- Q* <= (not 1) or (gated_S nand P) = 0 or gated_S nand P = gated_S nand P
		-- Q* <= (not 0) or (gated_S nand P) = 1 or gated_S nand P = 1
		-- Works!
