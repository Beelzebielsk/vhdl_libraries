#!/bin/bash

files="$@"
for f in $files; do
	sed -nre '/^entity/, /^end entity/{
		s/entity/component/
		p
	}' $f;
done | xclip -selection cb -i

